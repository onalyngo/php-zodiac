<html>
<head>
	<meta charset="UTF-8">
	<title>Landing Page</title>
	<link rel="stylesheet" href="https://bootswatch.com/4/solar/bootstrap.css">
</head>
<body>
	<div class="d-flex flex-column justify-content-center align-items-center vh-100 text-primary">
		<div class="bg-dark vh-70 rounded d-flex flex-column justify-content-center align-items-center">
			<?php
				session_start();
			?>
			<h1>Hello 
				<?php 
					echo $_SESSION['fullName']. "!"
				?>		
			</h1>
			<h1>Your zodiac sign is
				<span class="text-warning">
					<?php 
					echo $_SESSION['zodiac'] . "!"
					?>
				</span>
			</h1>
		</div>
	</div>
</body>
</html>