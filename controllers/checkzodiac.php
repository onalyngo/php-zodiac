<?php

	$fullName = $_POST['fullName'];
	$birthMonth = strtolower($_POST['birthMonth']);
	$birthDay = $_POST['birthDay'];

	// $months = ["January","February","March","April","May","June","July","August","September","October","November","December"];
	$zodiac = ["Aries", "Taurus", "Gemini", "Cancer", "Leo", "Virgo", "Libra", "Scorpio", "Sagittarius", "Capricorn", "Aquarius", "Pisces", ];

	if($birthMonth === "january" && $birthDay >=21){
		$zodiac = $zodiac[10];
	}else if($birthMonth === "february" && $birthDay <=19){
		$zodiac = $zodiac[10];
	}else if($birthMonth === "february" && $birthDay >=20){
		$zodiac = $zodiac[11];
	}else if($birthMonth === "march" && $birthDay <=20){
		$zodiac = $zodiac[11];
	}else if($birthMonth === "march" && $birthDay >=21){
		$zodiac = $zodiac[0];
	}else if($birthMonth === "april" && $birthDay <=20){
		$zodiac = $zodiac[0];
	}else if($birthMonth === "april" && $birthDay >=21){
		$zodiac = $zodiac[1];
	}else if($birthMonth === "may" && $birthDay <=21){
		$zodiac = $zodiac[1];
	}else if($birthMonth === "may" && $birthDay >=22){
		$zodiac = $zodiac[2];
	}else if($birthMonth === "june" && $birthDay <=21){
		$zodiac = $zodiac[2];
	}else if($birthMonth === "june" && $birthDay >=22){
		$zodiac = $zodiac[3];
	}else if($birthMonth === "july" && $birthDay <=22){
		$zodiac = $zodiac[3];
	}else if($birthMonth === "july" && $birthDay >=23){
		$zodiac = $zodiac[4];
	}else if($birthMonth === "august" && $birthDay <=22){
		$zodiac = $zodiac[4];
	}else if($birthMonth === "august" && $birthDay >=23){
		$zodiac = $zodiac[5];
	}else if($birthMonth === "september" && $birthDay <=23){
		$zodiac = $zodiac[5];
	}else if($birthMonth === "september" && $birthDay >=24){
		$zodiac = $zodiac[6];
	}else if($birthMonth === "october" && $birthDay <=23){
		$zodiac = $zodiac[6];
	}else if($birthMonth === "october" && $birthDay >=24){
		$zodiac = $zodiac[7];
	}else if($birthMonth === "november" && $birthDay <=22){
		$zodiac = $zodiac[8];
	}else if($birthMonth === "november" && $birthDay >=23){
		$zodiac = $zodiac[8];
	}else if($birthMonth === "december" && $birthDay <=21){
		$zodiac = $zodiac[9];
	}else if($birthMonth === "december" && $birthDay >=22){
		$zodiac = $zodiac[9];
	}

	session_start();
	if(strlen($fullName)===0 || strlen($birthMonth)===0 || strlen($birthDay)===0 || $birthDay > 31 || $zodiac==""){
		$_SESSION["errorMsg"] = "Please fill up the form properly.";
		header("Location: ". $_SERVER['HTTP_REFERER']);
	}else{
		$_SESSION['fullName'] = $fullName;
		$_SESSION['zodiac'] = $zodiac;
		header("Location: ../views/landingpage.php");
	}



?>