<html>
<head>
	<meta charset="UTF-8">
	<title>Zodiac Sign PHP Activity</title>
	<link rel="stylesheet" href="https://bootswatch.com/4/solar/bootstrap.css">
</head>
<body>
	<h1 class="text-center text-primary my-5">Welcome to Astrology!</h1>
	<div class="col-lg-4 offset-lg-4">
		<form class="bg-light p-4" action="controllers/checkzodiac.php" method="POST">
			<div class="form-group">
				<label for="fullName">Full Name</label>
				<input type="text" name="fullName" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthMonth">Birth Month</label>
				<input type="text" name="birthMonth" class="form-control">
			</div>
			<div class="form-group">
				<label for="birthDay">Birth Day</label>
				<input type="number" name="birthDay" class="form-control">
			</div>
			<div class="text-center">
				<button type="submit" class="btn btn-success">
					Check Zodiac Sign
				</button>
			</div>
			<?php
				session_start();
				session_destroy();
				if(isset($_SESSION['errorMsg'])){
			?>		
			<p class="text-danger text-center p-2"> <?php echo $_SESSION['errorMsg']?> </p>
			<?php
				}
			?>
		</form>
	</div>


</body>
</html>